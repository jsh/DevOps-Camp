How to setup DevStack on Delimiter.SoFree.Us

Provision the machine with Ubuntu 16.04

```
ssh osadmin@delimiter.sofree.us
su -
visudo # add nopasswd: to sudo group
sudo usermod -aG sudo osadmin
exit # root
exit # osadmin
# ---
ssh osadmin@delimiter.sofree.us
sudo apt-get update && sudo apt-get -y dist-upgrade && sudo apt-get -y autoremove
sudo reboot
# ---
ssh osadmin@delimiter.sofree.us
mkdir git
cd $_
sudo apt install git
git clone https://github.com/openstack-dev/devstack.git
cd devstack/
git checkout stable/queens
less local.conf # confirm local.conf
./stack.sh
```
