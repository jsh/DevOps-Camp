wget http://cloud-images.ubuntu.com/minimal/daily/bionic/current/bionic-minimal-cloudimg-amd64.img
openstack image create bionic-minimal --file bionic-minimal-cloudimg-amd64.img --disk-format qcow2 --container-format bare --public

wget https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img
openstack image create bionic-server --file bionic-server-cloudimg-amd64.img --disk-format qcow2 --container-format bare --public
