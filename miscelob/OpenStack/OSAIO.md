OpenStack-Ansible AIO
=====================

[quickstart](https://docs.openstack.org/openstack-ansible/latest/user/aio/quickstart.html)

Install CentOS 7, for compatibility with FusionIO iomemory devices in multi-node cluster.

```bash
# from workstation:
ssh-copy-id osadmin@OSA-AIO-C7
ssh osadmin@OSA-AIO-C7
sudo su -
```

Run this batch of commands on Ubuntu Server as root one line at a time.

```bash
# delete aio1 from /etc/hosts
sed -e '/aio1/d' -i /etc/hosts
echo 127.0.1.1 aio1 >> /etc/hosts
cat /etc/hosts
hostnamectl set-hostname aio1

# Ubuntu
apt update && apt -y dist-upgrade && apt -y autoremove && reboot

#CentOS
# Use nmtui or nmcli to enable enp0s3 to start on boot
yum install -y git epel-release && yum install -y python-pip
# get modern pip BEFORE the playbooks *fail*
pip install --upgrade pip && pip --version
yum -y upgrade && reboot
```

Then, run this batch of commands on the server as root one line at a time.

```bash
git clone https://git.openstack.org/openstack/openstack-ansible /opt/openstack-ansible
cd /opt/openstack-ansible/
# git checkout stable/queens
scripts/bootstrap-ansible.sh
scripts/bootstrap-aio.sh
cd playbooks/
openstack-ansible setup-hosts.yml
openstack-ansible setup-infrastructure.yml
openstack-ansible setup-openstack.yml
```
