{
  "AWSTemplateFormatVersion": "2010-09-09",

  "Description": "Autoscaling group n' stuff",

  "Parameters": {
    "Environment": {
      "AllowedValues": [ "Dev", "Prod" ],
      "Type": "String",
      "Default": "Dev"
    },
    "Ec2KeyPair": {
      "Description": "EC2 key pair for SSH access to servers (must be manually created prior to creating stack)",
      "Type": "AWS::EC2::KeyPair::KeyName"
    },
    "InstanceType": {
      "Type" : "String",
      "Description" : "Size of EC2 instances",
      "AllowedValues" : [
        "t2.micro",
        "t2.small",
        "t2.medium"
      ],
      "Default" : "t2.small"
    },
    "GroupSize": {
      "Type" : "Number",
      "Description" : "Number of instances for this scaling group",
      "Default" : 1,
      "AllowedValues": [ 1, 2, 3 ]
    },
    "StackNameVpc": {
      "Description": "Name of the stack containing the network and security group (type carefully)",
      "Type": "String"
    },
    "StackNameDb": {
      "Description": "Name of the stack containing the database for our webapp",
      "Type": "String"
    }
  },

  "Outputs": {
  },

  "Mappings": {
    "RegionMap": {
      "us-east-1": { "AMI": "ami-5cc39523", "1": "us-east-1a", "2": "us-east-1c", "1s": "ue1a", "2s": "ue1c" },
      "us-east-2": { "AMI": "ami-67142d02", "1": "us-east-2a", "2": "us-east-2b", "1s": "ue2a", "2s": "ue2b" },
      "us-west-2": { "AMI": "ami-39c28c41", "1": "us-west-2a", "2": "us-west-2b", "1s": "uw2a", "2s": "uw2b" }
    }
  },

  "Conditions": {
  },

  "Resources": {

    "InstanceRole" : {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Version" : "2012-10-17",
          "Statement": [ {
            "Action": [ "sts:AssumeRole" ],
            "Effect": "Allow",
            "Principal": { "Service": [ "ec2.amazonaws.com" ] }
          } ]
        },
        "Path": "/",
        "Policies" : [

        ]
      }
    },

    "InstanceProfile" : {
      "Type": "AWS::IAM::InstanceProfile",
      "Properties": {
        "Path": "/",
        "Roles": [ { "Ref" : "InstanceRole" } ]
      }
    },

    "LoadBalancer" : {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "Scheme" : "internet-facing",
        "Listeners" : [
          {
            "InstancePort" : "8000",
            "InstanceProtocol" : "TCP",
            "LoadBalancerPort" : "80",
            "Protocol" : "TCP"
          }
        ],
        "HealthCheck": {
          "Target" : "HTTP:8000/",
          "Interval" : "15",
          "Timeout" : "5",
          "HealthyThreshold" : "2",
          "UnhealthyThreshold" : "2"
        },
        "SecurityGroups" : [
          { "Fn::ImportValue": { "Fn::Sub": "${StackNameVpc}-${Environment}SecurityGroup" } },
          { "Fn::ImportValue": { "Fn::Sub": "${StackNameVpc}-PubWebSecurityGroup" } }
        ],
        "Subnets": [
          { "Fn::ImportValue": { "Fn::Sub": "${StackNameVpc}-PubSub1" } },
          { "Fn::ImportValue": { "Fn::Sub": "${StackNameVpc}-PubSub2" } }
        ]
      }
    },

    "LaunchConfig" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Properties" : {
        "AssociatePublicIpAddress" : false,
        "KeyName" : { "Ref" : "Ec2KeyPair" },
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ] },
        "IamInstanceProfile" : { "Ref" : "InstanceProfile" },
        "SecurityGroups" : [
          { "Fn::ImportValue": { "Fn::Sub": "${StackNameVpc}-${Environment}SecurityGroup" } },
          { "Fn::ImportValue": { "Fn::Sub": "${StackNameVpc}-AdminSshSecurityGroup" } }
        ],
        "InstanceType" : { "Ref" : "InstanceType" },
        "BlockDeviceMappings" : [
           { "DeviceName" : "/dev/sda1", "Ebs" : { "VolumeSize" : "8", "VolumeType" : "gp2", "DeleteOnTermination" : "true" } },
           { "DeviceName" : "/dev/sdb", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true"} }
        ],
        "UserData" : { "Fn::Base64" : { "Fn::Join" : [ "", [
          "#!/bin/bash -v\n\n",

          "# Set the hostname\n",
          "INSTANCE_ID=$( curl --silent http://169.254.169.254/latest/meta-data/instance-id | tail -c8 )\n",
          "NAME=", { "Ref" : "AWS::StackName" }, "-$INSTANCE_ID\n",
          "echo $(curl http://169.254.169.254/latest/meta-data/local-ipv4) $NAME >> /etc/hosts\n",
          "hostnamectl set-hostname $NAME\n\n",

          "# Apply all available updates to the server\n",
          "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 10; done\n",
          "apt update\n\n",

          "# Install AWS tools to notify Cloudformation when the instance is up and ready\n",
          "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 10; done\n",
          "apt install -y python-pip\n",
          "#apt install -y python-setuptools \n",
          "#mkdir aws-cfn-bootstrap-latest\n",
          "#curl https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz | tar xz -C aws-cfn-bootstrap-latest --strip-components 1 \n",
          "#ln -s /aws-cfn-bootstrap-latest/init/ubuntu/cfn-hup /etc/init.d/cfn-hup\n",
          "pip install https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz\n",
          "SIGNAL=\"/usr/local/bin/cfn-signal --stack ", { "Ref" : "AWS::StackName" }, " --resource ScalingGroup --region ", { "Ref" : "AWS::Region" }, "\"\n",
          "echo \"@reboot root while ! $SIGNAL; do sleep 1; done && rm /etc/cron.d/signal_cloudformation\" > /etc/cron.d/signal_cloudformation\n\n",

          "# Mount extra storage for Docker\n",
          "mkfs.ext4 /dev/xvdb\n",
          "echo /dev/xvdb /var/lib/docker ext4 defaults,discard 0 0 >> /etc/fstab\n",
          "mkdir -p /var/lib/docker && mount /var/lib/docker\n\n",

          "# Install Docker\n",
          "while [ ! -x /usr/bin/docker ]; do curl -sSL https://get.docker.com/ | sh; done\n",
          "# TODO: Fix this stuff if we need Docker credentials!\n",
          "# aws --region us-west-2 s3 cp s3://BUCKET/docker.tar.gz /etc/docker.tar.gz\n",
          "# tar xvfz /etc/docker.tar.gz --directory /etc/docker/\n\n",

          "# Create a cron job to clean up Docker cruft\n",
          "echo \"docker container prune -f\" > /etc/cron.hourly/docker_cleanup\n",
          "chmod +x /etc/cron.hourly/docker_cleanup\n\n",

          "# Reboot, which will trigger the cfn-signal we arranged above\n",
          "reboot\n"
        ] ] } }
      }
    },

    "ScalingGroup" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "DependsOn" : "LoadBalancer",
      "CreationPolicy": {
        "ResourceSignal": {
          "Count": { "Ref" : "GroupSize" },
          "Timeout": "PT30M"
        }
      },
      "Properties" : {
        "AvailabilityZones" : [
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "1" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "2" ] }
        ],
        "LaunchConfigurationName" : { "Ref" : "LaunchConfig" },
        "MinSize" : { "Ref" : "GroupSize" },
        "MaxSize" : { "Ref" : "GroupSize" },
        "MetricsCollection": [ { "Granularity": "1Minute", "Metrics": [ "GroupMinSize", "GroupMaxSize" ] } ],
        "VPCZoneIdentifier" : [
          { "Fn::ImportValue": { "Fn::Sub": "${StackNameVpc}-PrivSub1" } },
          { "Fn::ImportValue": { "Fn::Sub": "${StackNameVpc}-PrivSub2" } }
        ],
        "Tags" : [ {
          "Key" : "Name",
          "Value" : { "Fn::Join" : [ "-", [ { "Ref" : "Environment" }, { "Ref" : "AWS::StackName" }, "asg" ] ] },
          "PropagateAtLaunch" : "true"
        },{
          "Key" : "Env",
          "Value" : { "Ref" : "Environment" },
          "PropagateAtLaunch" : "true"
        } ],
        "LoadBalancerNames" : [ { "Ref" : "LoadBalancer" } ]
      }
    }

  }
}
