# Infrastructure as Code

#### What are we going to do in this lab?

Use infrastructure templates to build an environment for our application.

#### Why do we need to do this?

Hand-built infrastructure is dangerous.  It tends to be unique and poorly documented.  Changes can be difficult to control.  Infrastructure as code is versioned and repeatable.

#### When do you use it?

Always.  Kinda.

I find that manual prototyping is still very effective, especially when I need to figure out which options a GUI builds into a particular piece of infrastructure.

- Development:
  - 50% manual
- Quality Assurance:
  - 10% manual
- User Acceptance Testing:
  - 1% manual
- Production:
  - 0.1% manual

---

Amazon gives us very granular controls.  This is great, but it means that there's a ton we have to do.

Before you start, create a repository to house your infrastructure code:

`repo_setup.sh webapp-infrastructure`

Copy the JSON files from this lab into your repo.

---

# Part I: Network

First, we build out a network infrastructure:

- Private VPC
- Two private subnets spanning two availability zones
- Two public subnets spanning the same two availability zones
- A NAT Gateway in each public subnet to permit egress from the private zones
- An internet gateway to allow traffic in/out of our VPC
- Route tables to tie it all together


Fortunately, infrastructure as code!

---

Before we really dive in, create an EC2 key pair:

1. Log in to the AWS Console and go to EC2
1. Find `Key Pairs` in the left side nav menu
1. Click `Create Key Pair`
1. Name it after your macguffin
1. With little fanfare, it will create the key pair and your browser will download the private key (maybe silently)
1. `mv ~/Downloads/macguffin.pem ~/.ssh/` (_Substitute your macguffin name_)
1. `chmod 0600 ~/.ssh/macguffin.pem`


And let's set up access keys:

1. Go to the AWS IAM Console
1. Go to `Users` and find your own account
1. Go to the `Security Credentials` tab
1. Look for the `Access Keys` section, and click the `Create access key` button
1. Save these somewhere!
1. Run `aws configure` on the command line, adding the keys you just created (not the EC2 key pair)
1. Run `aws ec2 describe-regions` to verify that things are set up (you should see a JSON list of ~15 AWS regions)

---

*Now* we can dive in to infrastructure as code!

#### Lay down the network

- Go to AWS Console --> CloudFormation
- Click the `Create Stack` button
- Select Template
  - Choose the `Upload a template to Amazon S3` option
  - Pick `eip_reservations.json` from this folder
- Specify Details
  - Name your stack `macguffin-ips` (e.g. `ark-ips`)
- Options
  - You don't need to do anything on this page
- Review
  - Review what will be created and click `Create`

That's a minimal template.  You only had to give it a name.  The next one requires a few more decisions.

Perform the same steps as above, but this time for the `aws_base_network.json` template.

 - Select Template
   - Choose the `Upload a template to Amazon S3` option
   - Pick `aws_base_network.json` from this folder
 - Specify Details
   - Name your stack `macguffin-vpc` (e.g. `ark-vpc`)
   - AdminIP: Enter the results of this command `echo $(curl --silent ifconfig.co)/32`
   - StartingIP: Pick a number!  Any number!  (`16` to `31`, inclusive)
   - StaticIpStackName: Enter the name of your other stack, exactly: `static-ips`
 - Options
   - You don't need to do anything on this page
 - Review
   - Review what will be created and click `Create`
 - This stack will take longer to create.  You can monitor it from the CloudFormation console.

###### Discussion point

- Do we use CI for this?

---

# Part II: Servers

#### Jumphost

We're going to create a jump host to give us access to the things in our private (non-routable) subnets.  We will use some SSH trickery to bounce off this machine and into our private network.

And now we create our stack from `jumphost.json`:
- Specify Details
  - Stack name: `macguffin-jumphost` (e.g. `ark-jumphost`)
  - Ec2KeyPair: _your macguffin name_
  - Environment: Dev
  - VpcStackName: `macguffin-vpc` (e.g. `ark-jumphost`; Must match what you created above)
- Review
  - Check the box for `I acknowledge that AWS CloudFormation might create IAM resources.`
- In the CloudFormation console, select your jumphost stack and select the `Outputs` tab.  When your stack is complete, you will find a `PublicIP` here.  Record this (or just remember that it's here) for near-future use.

---

#### Postgres

You can proceed with the `postgres.json` template while the VPN server continues to build:

- Specify Details
  - Stack name: `macguffin-pgserver` (e.g. `ark-pgserver`)
  - AdminPass: `abc123def456` <<< This sucks.  Not a good way to obfuscate this in CloudFormation.
  - AdminUser: `pgadmin`
  - Ec2KeyPair: _your macguffin name_
  - Environment: `dev`
  - VpcStackName: `macguffin-vpc`

---

#### Scaling Group (Finally)

The deploy token we created in the last lab?  We're going to use it here:

```
MACGUFFIN=ark
aws --region us-west-2 secretsmanager create-secret --name ${MACGUFFIN}-deploy --description "Deploy token for ${MACGUFFIN}" --secret-string '{"user":"gitlab+deploy-token-10400","pass":"CTrofjWpZHk2-zrWzzj-"}'
```

Use the `autoscale_w_ansible.json` template

- Specify Details
  - Stack name: `macguffin-webapp` (e.g. `ark-webapp`)
  - DbPass: `abc123def456`
  - DbUser: `pgadmin`
  - GitlabSecretARN: The value of `aws secretsmanager get-secret-value --secret-id ${MACGUFFIN}-deploy | jq -r '.ARN'` (unless you modified that `create-secret` command up there ^^^)
  - Ec2KeyPair: _your macguffin name_
  - Environment: `dev`
  - GroupSize: `1`
  - InstanceType: `t2.small`
  - StackNameDb: `macguffin-pgserver`
  - StackNameVpc: `macguffin-vpc`
  - WebappImage: `registry.gitlab.com/happycampers/webapp-demo` <<< Get this from your repo
  - WebappVersionTag: `1aeb95b3` <<< Find the most recent build from your repo
    - Note: DO NOT USE `latest` for this.

This could very likely require some troubleshooting.

---

# Part III: Troubleshooting

#### SSH Through a Jumphost

Remember that PublicIP for the jumphost?  No?  Go back up a couple blocks and get it.

Run this to configure your system to use the jump host:

```
PUB_IP=PUT_THE_PUBLIC_IP_HERE
echo "Host 172.*" >> ~/.ssh/config
echo "\tProxyCommand ssh ubuntu@${PUB_IP} -W %h:%p"
```

Now configure your system to automatically provide your SSH key:

```
eval $(ssh-agent)
ssh-add ~/.ssh/macguffin.pem
```

No you can SSH into your machines easy-peasy!

Find the IP of one of your webapp machines:

- Either
  - CloudFormation --> webapp stack --> Resources --> LoadBalancer (click the `PhysicalId`) --> Instances --> click an `InstanceId`
- Or
  - EC2 console --> search for `macguffin-webapp`
- Select a machine and note its Private IP (e.g. `172.17.20.39`)

Now:

`ssh ubuntu@PRIVATE_IP` (e.g. `ssh ubuntu@172.17.20.39`)

This will use your EC2 SSH key, because of `ssh-agent`.  It will use the jump host because of what we added to `~/.ssh/config`.

---

#### Checking our Webapp

Now that you can SSH, you need to get in to one of your EC2 hosts and initialize the database:
- SSH in to one of your autoscaled webapp servers
  - `sudo docker exec webapp python manage.py migrate`
  - `sudo docker exec -it webapp python manage.py createsuperuser`
  - `curl localhost:8000`
- Find your load balancer
  - Check the `Instances` tab; make sure that one or more instances are `InService`
  - Find the DNS name of your load balancer.  It should be something pretty like:
    - `your_macguffin-webap-LoadBala-8VYGE90O3R4W-1411515677.us-west-2.elb.amazonaws.com`
    - Pull that up in a web browser.
    - Use the `/admin` endpoint to log in with the superuser account and create a poll.
    - Check the `/polls` endpoint

---

# Part IV: Auto(scaling) Abuse

If you looked at the `autoscaling_w_ansible.json` template and/or examined the autoscaling group in the console, you'll notice that min=max.  Why would we do this?

- Go to EC2 --> Instances and find one of your webapp servers (e.g. `Dev-ark-webapp2-asg`)
- Using the right-click context menu or the `Actions` drop-down menu, select `Instance State` --> `Terminate`
- Go to EC2 --> Auto Scaling Groups and find your auto scaling group
  - Check the Monitoring tab.  What do you see?
  - Check the Instances tab.  What do you see?  You may have to wait a minute and/or click the refresh button.
- Go to EC2 --> Load Balancers and find your load balancer
  - Check the Monitoring tab.  What do you see?
  - Check the Instances tab.  What do you see?  You may have to wait a minute and/or click the refresh button.


- Go back to the CloudFormation console
- Select your webapp stack
- `Actions` --> `Update stack`
- Select Template
  - Use current template
- Specify Details
  - Change group size to `3`
- Now do you see why we don't want to use `latest` as the Docker image tag for the webapp?

Note: You have to be careful what you update.
- Some resources can be updated
- Some must be replaced
- Some don't have an impact
  - To see this one in action, try changing the `WebappVersionTag` parameter in the webapp stack...
- These are documented in the [AWS CloudFormation reference](http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/template-reference.html)

---

### Considerations

- Because we're bootstrapping this cluster, we don't want to make changes after it starts.
- Rebuild rather than modify.


### Questions/Discussion

- What if we want to run another Docker container on this cluster?
  - Ansible could make it happen, but needs to manage ports and Load Balancers, too.

- Where do we store the autoscaling CF template?
  - With our webapp?
    - We don't want to deploy infrastructure changes every time we deploy the app.
    - We don't want to include our CF template in our Docker image.
  - In a central CF template repo?
    - That's not very micro-service-y.
  - In a separate `<app_service>-infrastructure` repo?

---

### Teardown

- Since we have our infrastructure as code, let's tear some of this down to save money!
- Leave the VPC stack.  (Which means leaving the static IP stack).
- Use the CloudFormation console to delete each other stack.
