{
  "AWSTemplateFormatVersion": "2010-09-09",

  "Description": "Standard VPC utilizing 2 Availablity Zones and NAT Gateways",

  "Parameters": {
    "AdminIP": {
      "Type": "String",
      "Description": "CIDR range of IP addresses that will be used to administer the cluster",
      "Default": "ww.xx.yy.zz/32"
    },
    "StartingIp": {
      "Description": "Starting IP address for the VPC (will be the X in 172.X.0.0/16, between 16-31)",
      "Type": "Number",
      "MinValue": "16",
      "MaxValue": "31"
    },
    "StaticIpStackName": {
      "Description": "Name of the stack containing the static IP's for the NAT gateways (type carefully)",
      "Type": "String"
    }
  },

  "Outputs": {
    "VpcRange": {
      "Value": { "Fn::Join": [ ".", [ "172", { "Ref": "StartingIp" }, "0.0/16" ] ] }
    },
    "VpcId": {
      "Value": { "Ref": "theVPC" },
      "Export": { "Name": { "Fn::Sub": "${AWS::StackName}-VpcId" } }
    },
    "PublicSubnetRange1": {
      "Value": { "Fn::Join": [ ".", [ "172", { "Ref": "StartingIp" }, { "Fn::FindInMap": [ "SubnetIp", "pub", "1" ] },  "0/23" ] ] }
    },
    "PublicSubnetId1": {
      "Value": { "Ref": "PublicSubnet1" },
      "Export": { "Name": { "Fn::Sub": "${AWS::StackName}-PubSub1" } }
    },
    "PublicSubnetRange2": {
      "Value": { "Fn::Join": [ ".", [ "172", { "Ref": "StartingIp" }, { "Fn::FindInMap": [ "SubnetIp", "pub", "2" ] },  "0/23" ] ] }
    },
    "PublicSubnetId2": {
      "Value": { "Ref": "PublicSubnet2" },
      "Export": { "Name": { "Fn::Sub": "${AWS::StackName}-PubSub2" } }
    },
    "PrivateSubnetRange1": {
      "Value": { "Fn::Join": [ ".", [ "172", { "Ref": "StartingIp" }, { "Fn::FindInMap": [ "SubnetIp", "priv", "1" ] },  "0/23" ] ] }
    },
    "PrivateSubnetId1": {
      "Value": { "Ref": "PrivateSubnet1" },
      "Export": { "Name": { "Fn::Sub": "${AWS::StackName}-PrivSub1" } }
    },
    "PrivateSubnetRange2": {
      "Value": { "Fn::Join": [ ".", [ "172", { "Ref": "StartingIp" }, { "Fn::FindInMap": [ "SubnetIp", "priv", "2" ] },  "0/23" ] ] }
    },
    "PrivateSubnetId2": {
      "Value": { "Ref": "PrivateSubnet2" },
      "Export": { "Name": { "Fn::Sub": "${AWS::StackName}-PrivSub2" } }
    },
    "DevSecurityGroup": {
      "Value": { "Ref": "DevSecurityGroup" },
      "Export": { "Name": { "Fn::Sub": "${AWS::StackName}-DevSecurityGroup" } }
    },
    "ProdSecurityGroup": {
      "Value": { "Ref": "ProdSecurityGroup" },
      "Export": { "Name": { "Fn::Sub": "${AWS::StackName}-ProdSecurityGroup" } }
    },
    "AdminSshSecurityGroup": {
      "Value": { "Ref": "AdminSshSecurityGroup" },
      "Export": { "Name": { "Fn::Sub": "${AWS::StackName}-AdminSshSecurityGroup" } }
    },
    "PubWebSecurityGroup": {
      "Value": { "Ref": "PubWebSecurityGroup" },
      "Export": { "Name": { "Fn::Sub": "${AWS::StackName}-PubWebSecurityGroup" } }
    }
  },

  "Mappings": {
    "Region2AZ": {
      "us-east-1": { "1": "us-east-1a", "2": "us-east-1c", "3": "us-east-1e", "1s": "ue1a", "2s": "ue1c", "3s": "ue1e" },
      "us-east-2": { "1": "us-east-2a", "2": "us-east-2b", "3": "us-east-2c", "1s": "ue2a", "2s": "ue2b", "3s": "ue2c" },
      "us-west-2": { "1": "us-west-2a", "2": "us-west-2b", "3": "us-west-2c", "1s": "uw2a", "2s": "uw2b", "3s": "uw2c" }
    },
    "SubnetIp": {
      "pub": { "1": "10", "2": "12", "3": "14" },
      "priv": { "1": "20", "2": "22", "3": "24"  }
    }
  },

  "Conditions": {
  },

  "Resources": {

    "theVPC": {
      "Type": "AWS::EC2::VPC",
      "Properties": {
        "CidrBlock": { "Fn::Join": [ "", [ "172.", { "Ref": "StartingIp" }, ".0.0/16" ] ] },
        "EnableDnsSupport": true,
        "EnableDnsHostnames": true,
        "Tags": [ { "Key": "Name", "Value": { "Ref": "AWS::StackName" } } ]
      }
    },

    "InternetGateway": {
      "Type": "AWS::EC2::InternetGateway"
    },

    "AttachGateway": {
      "Type": "AWS::EC2::VPCGatewayAttachment",
      "DependsOn": "InternetGateway",
      "Properties": {
        "VpcId": { "Ref": "theVPC" },
        "InternetGatewayId": { "Ref": "InternetGateway" }
      }
    },

    "PublicRouteTable": {
      "Type": "AWS::EC2::RouteTable",
      "Properties": {
        "VpcId": { "Ref": "theVPC" },
        "Tags": [
          { "Key": "Name", "Value": { "Fn::Join": [ "-", [ "rt", { "Ref": "AWS::StackName" }, "pub" ] ] } }
        ]
      }
    },

    "PrivateSubnet1RouteTable": {
      "Type": "AWS::EC2::RouteTable",
      "Properties": {
        "VpcId": { "Ref": "theVPC" },
        "Tags": [
          { "Key": "Name", "Value": { "Fn::Join": [ "-", [ "rt", { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "1s" ] }, { "Ref": "AWS::StackName" }, "priv1" ] ] } }
        ]
      }
    },

    "PrivateSubnet2RouteTable": {
      "Type": "AWS::EC2::RouteTable",
      "Properties": {
        "VpcId": { "Ref": "theVPC" },
        "Tags": [
          { "Key": "Name", "Value": { "Fn::Join": [ "-", [ "rt", { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "2s" ] }, { "Ref": "AWS::StackName" }, "priv2" ] ] } }
        ]
      }
    },

    "RouteToInternet": {
      "Type": "AWS::EC2::Route",
      "DependsOn": "AttachGateway",
      "Properties": {
        "RouteTableId": { "Ref": "PublicRouteTable" },
        "DestinationCidrBlock": "0.0.0.0/0",
        "GatewayId": { "Ref": "InternetGateway" }
      }
    },

    "PublicSubnet1": {
      "Type": "AWS::EC2::Subnet",
      "DependsOn": "theVPC",
      "Properties": {
        "VpcId": { "Ref": "theVPC" },
        "CidrBlock": { "Fn::Join": [ ".", [ "172", { "Ref": "StartingIp" }, { "Fn::FindInMap": [ "SubnetIp", "pub", "1" ] },  "0/23" ] ] },
        "AvailabilityZone": { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "1" ] },
        "Tags": [
          { "Key": "Name", "Value": { "Fn::Join": [ "-", [ { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "1s" ] }, { "Ref": "AWS::StackName" }, "pub1" ] ] } }
        ]
      }
    },

    "PublicSubnet1RouteTableAttachment": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": { "Ref": "PublicSubnet1" },
        "RouteTableId": { "Ref": "PublicRouteTable" }
      }
    },

    "PrivateSubnet1": {
      "Type": "AWS::EC2::Subnet",
      "DependsOn": "theVPC",
      "Properties": {
        "VpcId": { "Ref": "theVPC" },
        "CidrBlock": { "Fn::Join": [ ".", [ "172", { "Ref": "StartingIp" }, { "Fn::FindInMap": [ "SubnetIp", "priv", "1" ] },  "0/23" ] ] },
        "AvailabilityZone": { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "1" ] },
        "Tags": [
          { "Key": "Name", "Value": { "Fn::Join": [ "-", [ { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "1s" ] }, { "Ref": "AWS::StackName" }, "priv1" ] ] } }
        ]
      }
    },

    "PrivateSubnet1RouteTableAttachment": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": { "Ref": "PrivateSubnet1" },
        "RouteTableId": { "Ref": "PrivateSubnet1RouteTable" }
      }
    },

    "PublicSubnet2": {
      "Type": "AWS::EC2::Subnet",
      "DependsOn": "theVPC",
      "Properties": {
        "VpcId": { "Ref": "theVPC" },
        "CidrBlock": { "Fn::Join": [ ".", [ "172", { "Ref": "StartingIp" }, { "Fn::FindInMap": [ "SubnetIp", "pub", "2" ] },  "0/23" ] ] },
        "AvailabilityZone": { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "2" ] },
        "Tags": [
          { "Key": "Name", "Value": { "Fn::Join": [ "-", [ { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "2s" ] }, { "Ref": "AWS::StackName" }, "pub2" ] ] } }
        ]
      }
    },

    "PublicSubnet2RouteTable": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": { "Ref": "PublicSubnet2" },
        "RouteTableId": { "Ref": "PublicRouteTable" }
      }
    },

    "PrivateSubnet2": {
      "Type": "AWS::EC2::Subnet",
      "DependsOn": "theVPC",
      "Properties": {
        "VpcId": { "Ref": "theVPC" },
        "CidrBlock": { "Fn::Join": [ ".", [ "172", { "Ref": "StartingIp" }, { "Fn::FindInMap": [ "SubnetIp", "priv", "2" ] },  "0/23" ] ] },
        "AvailabilityZone": { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "2" ] },
        "Tags": [
          { "Key": "Name", "Value": { "Fn::Join": [ "-", [ { "Fn::FindInMap": [ "Region2AZ", { "Ref": "AWS::Region" }, "2s" ] }, { "Ref": "AWS::StackName" }, "priv2" ] ] } }
        ]
      }
    },

    "PrivateSubnet2RouteTableAttachment": {
      "Type": "AWS::EC2::SubnetRouteTableAssociation",
      "Properties": {
        "SubnetId": { "Ref": "PrivateSubnet2" },
        "RouteTableId": { "Ref": "PrivateSubnet2RouteTable" }
      }
    },

    "NAT1": {
      "DependsOn": "AttachGateway",
      "Type": "AWS::EC2::NatGateway",
      "Properties": {
        "AllocationId": { "Fn::ImportValue": { "Fn::Sub": "${StaticIpStackName}-EIP1" } },
        "SubnetId": { "Ref": "PublicSubnet1"}
      }
    },

    "NAT1Route": {
      "Type": "AWS::EC2::Route",
      "Properties": {
        "RouteTableId": { "Ref": "PrivateSubnet1RouteTable" },
        "DestinationCidrBlock": "0.0.0.0/0",
        "NatGatewayId": { "Ref": "NAT1" }
      }
    },

    "NAT2": {
      "DependsOn": "AttachGateway",
      "Type": "AWS::EC2::NatGateway",
      "Properties": {
        "AllocationId": { "Fn::ImportValue": { "Fn::Sub": "${StaticIpStackName}-EIP2" } },
        "SubnetId": { "Ref": "PublicSubnet2"}
      }
    },

    "NAT2Route": {
      "Type": "AWS::EC2::Route",
      "Properties": {
        "RouteTableId": { "Ref": "PrivateSubnet2RouteTable" },
        "DestinationCidrBlock": "0.0.0.0/0",
        "NatGatewayId": { "Ref": "NAT2" }
      }
    },

    "DevSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": "Security group for isolation of development resources",
        "SecurityGroupIngress": [
        ],
        "VpcId": { "Ref": "theVPC" },
        "Tags": [ { "Key": "Name", "Value": { "Fn::Join": [ "-", [ { "Ref": "AWS::StackName" }, "dev" ] ] } } ]
      }
    },

    "DevSecurityGroupSelfIngress": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": { "Ref": "DevSecurityGroup" },
        "IpProtocol": -1,
        "SourceSecurityGroupId": { "Ref": "DevSecurityGroup" }
      }
    },

    "ProdSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": "Security group for isolation of production resources",
        "SecurityGroupIngress": [
        ],
        "VpcId": { "Ref": "theVPC" },
        "Tags": [ { "Key": "Name", "Value": { "Fn::Join": [ "-", [ { "Ref": "AWS::StackName" }, "prod" ] ] } } ]
      }
    },

    "ProdSecurityGroupSelfIngress": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": { "Ref": "ProdSecurityGroup" },
        "IpProtocol": -1,
        "SourceSecurityGroupId": { "Ref": "ProdSecurityGroup" }
      }
    },

    "AdminSshSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": "Security group to allow SSH from the admin IP",
        "SecurityGroupIngress": [
          { "IpProtocol" : "TCP", "FromPort" : 22, "ToPort" : 22, "CidrIp" : { "Ref" : "AdminIP" } },
        ],
        "VpcId": { "Ref": "theVPC" },
        "Tags": [ { "Key": "Name", "Value": { "Fn::Join": [ "-", [ { "Ref": "AWS::StackName" }, "prod" ] ] } } ]
      }
    },

    "PubWebSecurityGroup": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": "Security group for public web access",
        "SecurityGroupIngress": [
          { "IpProtocol" : "TCP", "FromPort" : 80, "ToPort" : 80, "CidrIp" : "0.0.0.0/0" },
          { "IpProtocol" : "TCP", "FromPort" : 443, "ToPort" : 443, "CidrIp" : "0.0.0.0/0" }
        ],
        "VpcId": { "Ref": "theVPC" },
        "Tags": [ { "Key": "Name", "Value": { "Fn::Join": [ "-", [ { "Ref": "AWS::StackName" }, "prod" ] ] } } ]
      }
    }

  }
}
