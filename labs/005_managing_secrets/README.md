# public plans about shared secrets

### Suggestions

- Use the private ssh key for an EC2 keypair as an encryption key
- Encrypt Keepass databases for a project with same key file as the Ansible vaults.
- Rotate the encryption key file annually.
- Rotate the shared secrets as often as the sharing group changes.

### What kinds of secrets do we have?

- ssh identity files (some-private-key.pem)
- SSL identity files (some-server-cert.pem)
- Service admin credentials
- Equipment logins
- Database passwords
- password database symmetric keys (i.e. the secrets that decrypt Keepass databases and Ansible vaults)

### How do we manage secrets?

- As long as you DO NOT check in the passphrase or decryption key, it's OK to check in encrypted Keepass databases and Ansible Vaults.
- credentials meant for use by humans should be stored in Keepass databases
- credentials meant for use by computers should be stored in Ansible vaults
- whole files follow the same rules
  * a whole file meant for use by a human can be attached to a Keepass database record
  * a whole file meant for use by a computer can be encrypted with ansible-vault
- each project will have its own project-secrets.key which will unlock the Keepass databases and/or Ansible vaults associated with that project.
- *Never* email any secret.
- *Never* check any un-encrypted secret into git.
- Use voice, handwritten notes, USB sticks, or magic wormhole to transfer final secrets, including passwords and encryption keys.

### How to use encrypted secret databases

To open Ansible vaults do one of the following:

* `export ANSIBLE_VAULT_PASSWORD_FILE=/path/to/vault_password_file`
* add `--ask-vault-pass` to ansible* commands
* add `--vault-password-file==/path/to/vault_password_file` to ansible* commands

To encrypt a Keepass database, specify the key file and/or password, when creating it, like this:

![Keepass Opening](secrets.md.keepass.png)

### Let's try opening an encrypted Keepass database

Install and launch Keepass2

```bash
sudo apt install keepass2
# tap <Super> then type "keep", KeePass2 should appear, tap <Enter>
```

Open the example database in this directory using the passphrase: 'an example passphrase'

### Let's play with Ansible vaults.

There are two interesting ansible vaults in this directory, a variables file and an encrypted private key.

The passphrase is the same as the one for the Keepass database.

```bash
head anvault.yml
# decrypt the variables file
ansible-vault decrypt anvault.yml --ask-vault-pass
head anvault.yml
# re-encrypt the variables file
ansible-vault encrypt anvault.yml --ask-vault-pass
head anvault.yml

# now, the same thing, only faster
echo 'an example passphrase' > ~/.anvaultpass
chmod go-rwx ~/.anvaultpass
export ANSIBLE_VAULT_PASSWORD_FILE=~/.anvaultpass
head anvault.yml
ansible-vault decrypt anvault.yml
head anvault.yml
ansible-vault encrypt anvault.yml
head anvault.yml
```

For playbooks, use vaulted variables and files just like regular ones.

When you define ANSIBLE_VAULT_PASSWORD_FILE or provide one of the vault pass options, Ansible automatically decrypts any vaulted variables or files using the provided password.

```bash
head anvault.yml
head example-ssh-key.anvault
echo 'an example passphrase' > ~/.anvaultpass
chmod go-rwx ~/.anvaultpass
export ANSIBLE_VAULT_PASSWORD_FILE=~/.anvaultpass
ansible-vault view anvault.yml
ansible-vault view example-ssh-key.anvault
cat install-ssh-private-key-playbook.yml
ansible-playbook install-ssh-private-key-playbook.yml
```

What do you suppose it did? Hint: Look in your .ssh/
