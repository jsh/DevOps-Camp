# Django + Prometheus

This is complicated stuff.  We're adding code to our Django webapp project to tell it to expose metrics.  

https://github.com/korfuri/django-prometheus

- Go to your repo
  - `cd ~/git/${REPO_NAME}`
- Include `django-prometheus` in the build requirements
  - `echo django-prometheus >> ~/git/${REPO_NAME}/requirements.txt`
- Edit your settings
  - `atom ~/git/${REPO_NAME}/mysite/mysite/settings.py`
  - Tell your project to install `django_prometheus`
    ```
    INSTALLED_APPS = (
       ...
       'django_prometheus',
    )
    ```
  - Include the Django Prometheus middleware wrappers
    - The `...Before...` line must be the first entry
    - The `...After...` line must be the last entry
    ```
    MIDDLEWARE = (
        'django_prometheus.middleware.PrometheusBeforeMiddleware',
        ...
        'django_prometheus.middleware.PrometheusAfterMiddleware',
    )
    ```
  - Use the shim to gather database metrics
    - Find: `'ENGINE': 'django.db.backends.postgresql',`
    - Change to `'ENGINE': 'django_prometheus.db.backends.postgresql',`
- Add the URL's for the metrics
  - `atom ~/git/${REPO_NAME}/mysite/mysite/urls.py`
  - Add one line to your `urlpatterns`
    ```
    urlpatterns = [
        ...
        path('', include('django_prometheus.urls')),
    ]
    ```
- Configure your pod to enable Prometheus scraping
  - `atom ~/git/${REPO_NAME}/k8s/webapp-deploy`
  - Add this block to `.spec.template.metadata`
    ```
    annotations:
      prometheus.io/port: "8000"
      prometheus.io/scrape: "true"
    ```
- Save your files
- Push your changes
  - `git add -A`
  - `git commit -m 'integrate with prometheus'`
  - `git push`
- Wait for the build/deploy
- Check it out
  - `xdg-open http://webapp.dev.${MACGUFFIN}.sfsdevops.camp/metrics`
- You can check the status of the pod reporting by looking at the kubernetes-pods section of the Prometheus targets page
  - `xdg-open https://prometheus.dev.${MACGUFFIN}.sfsdevops.camp/targets#job-kubernetes-pods`
