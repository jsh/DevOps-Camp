Sensu setup, architecture

Client-described checks

#### Create an account for GitLab to access your cluster

```
SERVICE_NAME=gitlab-deploy
GCP_PROJECT=devopscampdemo

gcloud iam service-accounts create ${SERVICE_NAME} \
  --display-name "${SERVICE_NAME}" \
  --project ${GCP_PROJECT}

gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
  --member serviceAccount:${SERVICE_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com \
  --role roles/container.admin

gcloud iam service-accounts keys create \
  ~/Downloads/gcp_config/${SERVICE_NAME}-${GCP_PROJECT}.json \
  --iam-account ${SERVICE_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com
```

Copy the contents of `~/Downloads/gcp_config/${SERVICE_NAME}-${GCP_PROJECT}.json` to a variable in your repositry called `DEV_GCP_AUTH_KEYFILE`.


#### Create an account for Kubernetes to pull from your registry

Repository --> Settings --> Repository --> Deploy Token

`kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com \
  --docker-username="<token-name>" --docker-password="<token-pword>" --docker-email="nobody@example.com"`

Note: The name `regcred` has to match the ImagePullSecrets in our manifests.

#### 
