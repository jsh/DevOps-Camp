# Build our webapp in GitLab

#### What are we going to do in this lab?

Use GitLab CI to build and test our software.

#### Why do we need to do this?

We need the artifact (in this case a Docker image) available to our systems in development, test, and production.  Building it by hand is tedious, and we want to go fast.  Any CI system will do, but we're using GitLab CI because it's free and fully-featured.

#### When do you use it?

Any software built in your organization is going to need to go somewhere.  A CI system can be a quick n' dirty sanity check, or a thorough combing-through of your software.  Either way, it offers a reliable way to offload the build/test/deploy process and enforce standard practices.

---

### Change the remote for your webapp repository

Use the group and project you created earlier.

(You can copy the git link from the GitLab GUI.)

- Look at the current remote git repository
  - `git remote -v`
- Remove the remote
  - `git remote rm origin`
- Add your own repository
  - `git remote add origin git@gitlab.com:<your_group>/<your_project>.git`
- Verify the change
  - `git remote -v`
- Push your code to your own repository
  - `git push --set-upstream origin master`


### Build a Docker image using GitLab CI

- Copy the `.gitlab-ci.yml` file from this repository into your webapp repository
  - `cp ~/git/DevOps-Camp/labs/060_gitlab_container/.gitlab-ci.yml ~/git/${REPO_NAME}`
- Make sure you're in _your_ repository
  - `cd ~/git/${REPO_NAME}`
- Add all your modified files to the staging index
  - `git add -A`
- Create a new commit
  - `git commit -m 'initial commit; build via gitlab CI'`
- Push all local commits to the server
  - `git push`
- Investigate the GUI
  - `xdg-open https://gitlab.com/${GROUP_NAME}/${REPO_NAME}/pipelines`
- When your pipeline is finished, investigate the registry
  - `xdg-open https://gitlab.com/${GROUP_NAME}/${REPO_NAME}/container_registry`

---

#### Create a Deploy Token

We're going to need this later.

- Navigate to Settings --> Repository for your repository
  - `https://gitlab.com/${GROUP_NAME}/${REPO_NAME}/settings/repository`
- Expand `Deploy Tokens` and add a new token
  - Name it `<your_macguffin>-deploy`  (e.g. `ark-deploy`)
- Grant it `read_registry` scope
- *_SAVE THE USERNAME/PASSWORD IN KEEPASS_*
