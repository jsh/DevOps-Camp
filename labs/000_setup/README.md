Make sure that we have student machines set up.

This will be either git+Docker or Virtualbox + preconfigured OVA

## Docker

TODO

## VirtualBox

Install or update VirtualBox from www.virtualbox.org

Download an Ubuntu Bionic Learner Station from https://nextcloud.sofree.us/index.php/s/Kg3z3Z2dB5rOpnH or from a provided USB stick.

In VirtualBox, import the Learner Station with File, Import Appliance.

Sign in as osadmin with the usual password. If you don't know it, ask your instructor, email captains@sofree.us, or ask in [Mattermost](https://mattermost.sofree.us)

Create an administrative user account for yourself.

Using the GUI: Settings, Details, Users, Unlock, Add User..., Administrator, etc...

Or, using the CLI:

```bash
sudo adduser $USERNAME
sudo usermod -aG sudo $USERNAME
```

Sign in as yourself.

Fetch the class materials.

```bash
wget https://www.sofree.us/bits/git-a-class
less git-a-class
chmod +x git-a-class
./git-a-class DevOps-Camp
```

Create a keypair and authorize it to login to your account on the GitLab server.

```bash
ssh-keygen
cat ~/.ssh/id_rsa.pub
xdg-open https://gitlab.com/profile/keys
```

Install the rest of the class requirements.

```bash
less ~/git/DevOps-Camp/labs/000_setup/get-reqd
~/git/DevOps-Camp/labs/000_setup/get-reqd
```

If you don't have another preference, install Atom.io (the code editor from GitHub)

```bash
wget -O atom.deb https://atom.io/download/deb
sudo apt install -y gconf2
sudo dpkg -i atom.deb
```
