# Build a GKE cluster

#### What are we going to do in this lab?

Build a Kubernetes cluster in Google, using Google Kubernetes Engine (GKE).

#### Why do we need to do this?

I mean, c'mon!  We already deployed a K8s cluster in Amazon!

Yes, but Google made Borg.  Borg became Kubernetes.  There's not a ton of difference between Kubernetes on Google and Amazon (the authenticators, primarily), but this lab will show you two things:
1. Different styles of infrastructure as code
1. Portability of K8s-deployed services

#### When do you use it?

Google makes spinning up K8s clusters [phenomenally easy](https://www.youtube.com/watch?v=kOa_llowQ1c&t=72).  If you're already a Google Cloud customer or if you need a quick n' easy place to play around...

---

### IMPORTANT:

Make sure that you and your partner work together on this.  Decide which of you will build a development cluster, and which will build a production cluster.  There will be no functional difference between the clusters.

---

## Install gcloud

Make sure the Google Cloud SDK (gcloud) is installed.

https://cloud.google.com/sdk/docs/downloads-apt-get

- `export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"`
- `echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list`
- `curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -`
- `sudo apt-get update && sudo apt-get install google-cloud-sdk`

---

## Login + Make a Google Cloud Project

- Login and make a project
  - Name the project == your_macguffin
  - `export GCP_PROJECT=${MACGUFFIN}`
  - `gcloud init`
- Set your default project in your shell
  - `gcloud config set project ${GCP_PROJECT}`

Note: The `gcloud init` command performs the `gcloud auth login` command (which you will probably need in the near future) as well as offering you the option to create a new project.

---

## Launch a Cluster

- Log in to the console
  - [console.cloud.google.com](console.cloud.google.com)
- Select your project from the drop-down at the top
- Navigate to `Kubernetes Engine` console
- This is required to enable/activate GKE for your account

```
export NOW=$(date +%Y%m%d%H%M%S)
export CLUSTER_NAME=${MACGUFFIN}
export CLUSTER_REGION=us-central1
```

```
gcloud beta container clusters create --region "${CLUSTER_REGION}" "${CLUSTER_NAME}" \
  --username "admin" \
  --cluster-version "1.10.5-gke.3" \
  --machine-type "n1-standard-1" \
  --image-type "COS" \
  --disk-type "pd-standard" \
  --disk-size "100" \
  --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
  --num-nodes "1" \
  --no-enable-cloud-logging \
  --no-enable-cloud-monitoring \
  --enable-ip-alias \
  --network "default" \
  --subnetwork "default" \
  --addons HorizontalPodAutoscaling,HttpLoadBalancing \
  --no-enable-autoupgrade \
  --enable-autorepair
```

---

## kubectl

- Install
  - `sudo apt install kubectl` (it's in the Google repo we added above)
- Configure
  - `gcloud container clusters get-credentials ${CLUSTER_NAME} --region ${CLUSTER_REGION}`
- Verify
  - `kubectl get nodes` >>> Look for `${CLUSTER_NAME}` in the output
- Grant your account admin access:
  - `kubectl create clusterrolebinding cluster-admin-binding-${NOW} \
    --clusterrole cluster-admin \
    --user $(gcloud config get-value account)`

More information regarding the [RBAC implementation w/ GKE](https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control).

---

## Kubernetes Nginx Ingress

- Scaffolding for the ingress:
  - `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml`
- Remove the admin access for your account:
  - `kubectl delete clusterrolebinding cluster-admin-binding-${NOW}`
- Ingress controller service:
  - `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/cloud-generic.yaml`

---

### Add DNS for your cluster

- Use `kubectl -n ingress-nginx get svc ingress-nginx` to find the `EXTERNAL_IP` of your ingress controller.
- Work with instructors to get this added to DNS
- You should have DNS entries for `*.dev.MACGUFFIN.sfsdevops.camp` and `*.MACGUFFIN.sfsdevops.camp`

Note: If you manage DNS, consider automating this piece.  (Google's Cloud DNS, Amazon's Route53, and even GoDaddy all have API's you can program against.)  On the other hand, this might be something you don't do very often - especially if you use the `*.domain` format for your cluster.

---

### Configuration as Code!

This readme was really manual, right?  We might not want full CI for our GKE clusters.  Or we might, but that gets hard and it's a diversion from our main goal of creating a pipeline.  Either way, a good next step is to [convert all our manual steps to a script](https://gitlab.com/sofreeus/devopscamp-gke).
