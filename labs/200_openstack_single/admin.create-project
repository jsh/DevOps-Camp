#!/bin/bash -eu

# Make sure I'm logged in as admin!
echo "$OS_USERNAME at $OS_AUTH_URL"

if [[ $OS_USERNAME != 'admin' ]]
then
  echo "This script needs to run as admin, not $OS_USERNAME."
fi

if [[ -f $1 ]]
then
  source $1
else
  echo "USAGE: $0 project.settings"
fi

# Create a new project and get the id
openstack project create $project_name --description "$project_description"


# Create a new group for the project
openstack group create --description "$project_name admins" ${project_name}-admins

# Create the global user role if it does not exists
openstack role create --or-show user

# Add the user role to the project and group
openstack role add --project $project_name --group ${project_name}-admins user

# Create new users and add them to the admin group for the project
for user in $project_admins
do
  openstack user create --project $project_name --password $default_password --email $project_email $user
  openstack group add user ${project_name}-admins $user
done

# setup project network
openstack network create ${project_name}-net   --project ${project_name}
openstack subnet create ${project_name}-subnet --project ${project_name} --network ${project_name}-net --subnet-range $PROJECT_NET_CIDR
openstack router create ${project_name}-router --project ${project_name}
openstack router set --external-gateway public ${project_name}-router
openstack router add subnet ${project_name}-router ${project_name}-subnet
