# OpenStack

OpenStack is a free-libre datacenter software for provisioning private and public cloud infrastructure. It's sort of like AWS, GCP, or Azure, but it's open-source, like Linux.

## Create a new project

Get the admin password to the OpenStack cluster from an instructor, or ask the instructor to perform the following steps for you.

1. Sign in to [Horizon](http://delimiter.sofree.us), the OpenStack Dashboard, as 'admin'.
*  Download admin's RC file. Click the user control in the upper-right corner, and choose "OpenStack RC File v3".
*  Create the new project in the CLI:
  1. Build a `project.settings.private` file from a project.settings.(example).
  2. Source admin's RC file.
  3. Run `admin.create-project project.settings.private` to create a new project.
*  Verify project, users, network, and router in the GUI or CLI.
*  Assign two Elastic IP's to the projectIn the GUI.

## Create a new server

1. Sign in to Horizon as any of the project users just created and create an instance GUI'ly.
2. Compute > Instances > Launch instance
  * Details:
    + Instance name = 'alpha'
  * Source:
    + Delete volume... = yes
    + Choose the "Cirros..." image
  * Flavour:
    + Choose "m1.nano"


## Create a public ssh security group

## Associate an Elastic IP and the ssh security group



## Create and associate a public ssh .

As a user, add a keypair, three instances (a.k.a. servers): {app,db,admin}, two security groups (ssh (allows ssh), web (allows http/s), db (allows PGSQL & MySQL from 'web') ), associate the Elastic IPs with admin and web nodes.

tests:
- admins can ssh to the management node but not the app node
- management node can ssh to every other node, but no other node can ssh to any other node
- app node can mysql and pgsql to the db node, but the management node can't, and no other node can reach db at all
