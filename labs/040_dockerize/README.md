# Webapp in Docker!

#### What are we going to do in this lab?

We're going to build a Docker image to run our webapp.

#### Why do we need to do this?

- Very clean environment to run code
- More portable than a virtual machine
- Easier to install than a software package

#### When do you use it?

- No
  - GUI
  - Polyglot
- Yes
  - Anything else

For local software builds, it provides a blank slate every time.  It prevents "It builds on my machine!" syndrome.  And if it builds on one machine, it should build anywhere.  And it runs on one machine, it should run anywhere.

---

### Webapp Docker Image

- Copy the Dockerfile from this lab to your webapp-demo
  - `cp ~/git/DevOps-Camp/labs/040_dockerize/Dockerfile ~/git/ark_webapp`
- Build the Docker image
  - `docker build -t mywebapp .`
- Run a container based on the image
  - `docker run -d --name webapp -p8000:8000 -e DJANGO_DB_USER -e DJANGO_DB_PASS -e DJANGO_DB_HOST mywebapp`
- Let's check on it:
  - `docker logs webapp`
- We have a problem.  The DB host we set earlier won't work in a container.
  - `DJANGO_DB_HOST=localhost`
- Clean it up:
  - `docker stop webapp`

### Solution: Docker networks

- Create a local network for Docker containers
  - `docker network create localwebapp`
- We have to stop our PGSQL server and bring it up on the `localwebapp` network.
- We're going to delete it and re-create it, but that's okay because we have a volume housing the stateful data!
- Stop and remove it
  - `docker stop db && docker rm db`
- Add `--net=localwebapp` to the database container command, and drop `-p 5432:5432`:
  - `docker run --net=localwebapp -d --rm --name db -v pgsql_data:/var/lib/postgresql/data -e POSTGRES_PASSWORD postgres:10.4`
- Because we get free DNS with Docker networks:
  - `export DJANGO_DB_HOST=db`
- Bring up the webapp again
  - `docker run --net=localwebapp -d --rm --name webapp -p8000:8000 -e DJANGO_DB_USER -e DJANGO_DB_PASS -e DJANGO_DB_HOST mywebapp`

Try http://localhost:8000/polls and http://localhost:8000/admin

Or

`docker run --net=localwebapp appropriate/curl -Li webapp:8000/polls`
