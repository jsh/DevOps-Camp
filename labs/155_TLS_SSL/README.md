# Adding TLS/SSL on your ingress

#### What are we going to do in this lab?

We're going to learn how to secure our web-facing properties with TLS certificates that automatically renew via LetsEncrypt.

#### Why do we need to do this?

Starting in Chrome 68, the browser is warning users whenever they go to a non-TLS site.  This is table stakes.  TLS (f.k.a. SSL) ensures that communications between the client and ingress are encrypted.  Without it, information like passwords are transmitted in plain text.

#### When do you use it?

There is no decision point here.  Us it everywhere all the time.  The initial setup is a challenge, so you should work to add it to your cluster provisioning automation.  When you've got it included in the cluster by default, it should be the standard practice to configure every new service with TLS before it gets to production.

---

## Add a Let's Encrypt certificate manager to your cluster

#### Grant your account admin access

```
kubectl create clusterrolebinding cluster-admin-binding-${NOW} \
  --clusterrole cluster-admin \
  --user $(gcloud config get-value account)
```

#### cert-manager

```
CERT_MANAGER_MANIFEST="https://raw.githubusercontent.com/jetstack/cert-manager/release-0.4/contrib/manifests/cert-manager/with-rbac.yaml"
curl ${CERT_MANAGER_MANIFEST} | kubectl apply -f -
```

#### Remove the admin access for your account

`kubectl delete clusterrolebinding cluster-admin-binding-${NOW}`


#### Configure the ClusterIssuer resource for staging:

Note: This requires that you have DNS properly configured for your cluster.

`kubectl apply -f issuer-staging.yaml`

Check the logs for cert-manager to see what's going on:

`kubectl -n cert-manager get pod`

`kubectl -n cert-manager logs -f cert-manager-6bfc47569c-j9x9j`

---

## Add TLS to your site

Note: You can make changes and try to push them through CI, but that can be a very slow/tedious process.  My recommendation is to prototype locally to iterate more quickly.  When you think you have a working prototype, try to push it out via CI.

#### Add the TLS cert to your ingress

Note: We are using the 'ingress-shim' feature of cert-manager.  You may optionally [define a certificate separately](https://cert-manager.readthedocs.io/en/latest/reference/certificates.html).

Get a copy of your running ingress:

`kubectl get ing demo-webapp -o yaml > /tmp/ingress.yaml`

We're going to use `/tmp/ingress.yaml` for our prototyping, then add our changes back to the template when we're good.

Add this to the `.metadata` block of your ingress manifest:

```
annotations:
  certmanager.k8s.io/cluster-issuer: letsencrypt-staging
  kubernetes.io/tls-acme: "true"
```

Add this to the `.spec` block of your ingress manifest, and apply it.  Make sure it matches what's in your certificate manifest.

```
tls:
- hosts:
  - webapp.dev.${MACGUFFIN}.sfsdevops.camp
  secretName: ${MACGUFFIN}-dev-tls
```

It can take >10 minutes for your certificate to verify.

When the staging cert is A-OK, you'll see this in your browser:

![Functional Staging Certificate](working_staging_cert.png)


#### Switch to a real certificate

- `kubectl apply -f issuer-prod.yaml`
- In the `.metadata.annotations` of your ingress, change `letsencrypt-staging` to `letsencrypt-prod`
- Check the cert-manager logs again to watch for the cert.
Check the cert itself to make sure it's using the correct issuer
  - `kubectl get cert ${MACGUFFIN}-dev-tls -o yaml`
  - You may need to delete the secret and let it re-create
    - `kubect delete secret ${MACGUFFIN}-dev-tls`

You can also use `kubectl get secrets --watch`

---

## Get it through CI

Add the `annotations` and `tls` entries into your ingress template (making sure to change the hostname to `webapp.DOMAIN`).  Then push your changes and monitor the process in the CI/CD Pipeline.
