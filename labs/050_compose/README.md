# Let's compose ourselves

#### What are we going to do in this lab?

Start and stop Django and PostgreSQL with single command using docker-compose.

#### Why do we need to do this?

Docker is great, but sometimes running all the components for an application can be a pain.  Docker Compose supports more complex configurations, and provides an easy way to start and stop multi-container applications.

#### When do you use it?

Docker Compose is great for local testing and prototyping.  It's also a fantastic way to demonstrate your service to others: By showing them how your service interacts with other services, they can use that model to deploy your service in their own environment.

I highly discourage using Docker Compose in production.

---

Copy the `docker-compose.yml` from this lab into your webapp repo.

`cp ~/git/DevOps-Camp/labs/050_compose/docker-compose.yml ~/git/${REPO_NAME}`

It contains all the options from our previous `docker run ...` commands in one place.

Buuuut... we have to start over again.

So...

1. Run this in your webapp project root before you start:
  - `export COMPOSE_DIR=$(basename $(pwd) | tr -d '_')`

First run (or after `docker-compose down -v`):
1. Try to start
  - `docker-compose up`
1. You may see issues where the database doesn't start quickly enough for the webapp.  In that case, you can start them separately:
  - `docker-compose up -d db`
  - `docker-compose up -d webapp`
1. Update the DB with our models
  - `docker exec ${COMPOSE_DIR}_webapp_1 python manage.py migrate`
1. Create the admin account
  - `docker exec -it ${COMPOSE_DIR}_webapp_1 python manage.py createsuperuser`
1. Restart and see how things look
  - `docker-compose down`
  - `docker-compose up -d`

Subsequent runs:
1. Bring down the containers and network
  - `docker-compose down`
1. Launch the app in the background, with persistent data
  - `docker-compose up -d`

For testing:
1. Run the full suite of tests against the container
  - `docker exec ${COMPOSE_DIR}_webapp_1 python manage.py test polls`

Cleanup:
1. Remove containers, networks, _and volumes_
  - `docker-compose down -v`
