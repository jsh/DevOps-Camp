#!/usr/bin/env python

import requests, os

# Here's all my geocoding request data in a nice, readable format
args = { 'key': os.environ['MQ_API_KEY'], 'location': '1101 County Rd 53, Granby, CO 80446' }

# We make the request.  Like `curl` in the shell, but we store the result
geocode_data = requests.get( 'https://www.mapquestapi.com/geocoding/v1/address', params=args ).json()

# Pull the geo data out of the response
lat,lng = geocode_data['results'][0]['locations'][0]['latLng'].values()

# Here's all my elevation request data in a nice, readable format
elevation_url = 'http://open.mapquestapi.com/elevation/v1/profile'
args = { 'key': os.environ['MQ_API_KEY'], 'latLngCollection': f"{lat},{lng}", 'unit': 'f' }

# Make the request to the elevation endpoint and store the response
elev = requests.get( elevation_url, params=args ).json()['elevationProfile'][0]['height']

print( f"The elevation is {elev} feet" )
