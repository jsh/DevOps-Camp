#!/usr/bin/env bash

# Here's all my data ... the format isn't the prettiest
ADDR="1101+County+Rd+53%2C+Granby%2C+CO+80446"
GEOCODE="https://www.mapquestapi.com/geocoding/v1/address?key=${MQ_API_KEY}&location=${ADDR}"

# Make the request to the geocoding endpoint and store the response
GEOJUMBLE=$(curl -s "${GEOCODE}")

# Grab the latLng object
# This is easier than with `awk`, but it takes some trickery to convert the
# numbers to a string we can use.
LATLNG=$(echo ${GEOJUMBLE} | jq -r '.results[0].locations[0].latLng | (.lat|tostring)+","+(.lng|tostring)')

# Construct our URL for the elevation query
ELEVATION="http://open.mapquestapi.com/elevation/v1/profile?key=${MQ_API_KEY}&latLngCollection=${LATLNG}&unit=f"

# Make the request to the elevation endpoint and store the response
ELEJUMBLE=$(curl -s "${ELEVATION}")

# Grab the height
HEIGHT=$(echo ${ELEJUMBLE} | jq '.elevationProfile[0].height')

# Print the result
echo "The elevation is ${HEIGHT} feet (jq)"
