#!/usr/bin/env bash

# Here's all my data ... the format isn't the prettiest
ADDR="1101+County+Rd+53%2C+Granby%2C+CO+80446"
GEOCODE="https://www.mapquestapi.com/geocoding/v1/address?key=${MQ_API_KEY}&location=${ADDR}"

# Make the request to the geocoding endpoint and store the response
GEOJUMBLE=$(curl -s "${GEOCODE}")

# Parse latLng object
LATLNG=$(echo ${GEOJUMBLE} | awk -F{ '{print $9}' | awk -F\} '{print $1}')

# Parse latitude from the latLng object
LAT=$(echo ${LATLNG} | awk -F: '{print $2}' | awk -F, '{print $1}')

# Parse longitude from the latLng object
LNG=$(echo ${LATLNG} | awk -F: '{print $3}' | awk -F, '{print $1}')

# Construct our URL for the elevation query
ELEVATION="http://open.mapquestapi.com/elevation/v1/profile?key=${MQ_API_KEY}&latLngCollection=${LAT},${LNG}&unit=f"

# Make the request to the elevation endpoint and store the response
ELEJUMBLE=$(curl -s "${ELEVATION}")

# Parse the elevation from the string
HEIGHT=$(echo ${ELEJUMBLE} | awk -F: '{print $4}' | awk -F\} '{print $1}')

# Print the result
echo "The elevation is ${HEIGHT} feet (awk)"
