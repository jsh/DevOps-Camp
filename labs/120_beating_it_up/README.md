# Beating it up

#### What are we going to do in this lab?

We're going to bang on our service and see if we can make it fall over.

#### Why do we need to do this?

DevOps can involve a lot of challenging and painful incremental work.  Making sure that you've put your product (in this case the deployment configuration) through its paces is a good way to find flaws.

#### When do you use it?

When you're building a new service, test the snot out of it.  Test your assumptions.  Make sure things work like you expect them to.  If they don't, iterate until they do.  If that fails, rewrite the requirements.

---

### *Be* the street walking cheetah with a heart full of napalm

```
kubectl get pod -l app=demo-webapp,tier=web
WEBAPP_POD=$(kubectl get pod -l app=demo-webapp,tier=web --no-headers | awk '{print $1}')
kubectl delete pod ${WEBAPP_POD}
kubectl get pod
```

What just happened?

```
WEBAPP_POD=$(kubectl get pod -l app=demo-webapp,tier=web --no-headers | awk '{print $1}')
kubectl delete pod ${WEBAPP_POD} && kubectl get pod --watch
```

Still too fast to tell!

### What about the DB?

```
DB_POD=$(kubectl get pod -l app=demo-webapp,tier=db --no-headers | awk '{print $1}')
kubectl delete pod ${DB_POD}
kubectl get pod
```

Try to access the website.

What happened???  (Anything?)

```
kubectl get pvc
kubectl get pv
kubectl get sc standard -o yaml
```

---

### A New Storage

The default StorageClass has a reclaimPolicy of Delete.
Let's add another that will retain data.

- DO NOT COPY the `persistent-storage-class.yaml` into your webapp repo.
  - This is part of the cluster config, not your webapp config
  - `kubectl apply -f ~/git/DevOps-Camp/labs/120_beating_it_up/persistent-storage-class.yaml`
  - In the real world, we'll maintain a repo for our infrastructure service pieces.  We would have a Kubernetes repo that looks very similar to the 090_gke lab folder.

---

### Begin Again

We're swapping out the storage underpinning our database server.  Let's clean up the dependency chain to avoid collisions and complications.

- Delete our deploys
  - `kubectl delete deploy -l app=demo-webapp`
- Delete the (non) persistent volume claim
  - `kubectl delete pvc webapp-db-disk`
- Create the new (actually) persistent volume claim
  - `cp ~/git/DevOps-Camp/labs/120_beating_it_up/postgres-volume-claim.yaml ~/git/${REPO_NAME}/k8s/`
  - `kubectl apply -f postgres-volume-claim.yaml`
- Check out the storage
  - `kubectl get pvc webapp-db-disk`
  - `kubectl get pv`
  - It should say `Retain` instead of `Delete`
- Bring things up again.
  - `kubectl apply -f postgres-deploy.yaml`
  - `kubectl apply -f webapp-init-job.yaml`
  - `kubectl apply -f webapp-deploy.yaml`
  - `WEBAPP_POD=$(kubectl get pod -l app=demo-webapp,tier=web -o jsonpath='{.items[].metadata.name}')`
  - `kubectl exec -it ${WEBAPP_POD} python manage.py createsuperuser`
- Clean up the init job
  - `kubectl delete job webapp-init`

---

### Destroy again!

```
DB_POD=$(kubectl get pod -l app=demo-webapp,tier=db --no-headers | awk '{print $1}')
kubectl delete pod ${DB_POD}
kubectl get pod
```
