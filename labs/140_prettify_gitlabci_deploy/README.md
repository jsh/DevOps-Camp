# DRY

#### What are we going to do in this lab?

"DRY" is an acronym for "Don't Repeat Yourself."  We're going to take big blocks of repeated code from the last lab and wrap it into a script we can call.

#### Why do we need to do this?

This isn't absolutely necessary, but it helps keep things readable and reusable.  It lays the foundation for, say, moving some of your deployment code into a reusable submodule.

#### When do you use it?

When you find blocks of repeated code, look for a way to consolidate them into a single reusable block.  But also consider the tradeoffs.  In this case, does it hide part of our deployment process from CI logging?

---

Consolidate all your Kubernetes webapp (not Postgres) manifests in your webapp repo, in a directory called `k8s`.

DO NOT INCLUDE the `webapp-init-job.yaml` in the `k8s` directory.  Make a directory called `jobs` and put it there.

Your repo should look like this:
```
demo-webapp
├── Dockerfile
├── README.md
├── jobs
│   └── webapp-init-job.yaml
├── k8s
│   ├── webapp-deploy.yaml
│   ├── webapp-ingress.yaml
│   └── webapp-service.yaml
├── mysite
│   ├── manage.py
│   ├── mysite
│   └── polls
└── requirements.txt
```

Take a look at the updated `.gitlab-ci.yml` and make sure you have all the variables set correctly at the organization (group, not repo) level in GitLab.
