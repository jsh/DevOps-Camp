# Our Django webapp

#### What are we going to do in this lab?

This lab introduces a Django webapp for creating and managing polls.  The application is a good example of a multi-tier webapp.

#### Why do we need to do this?

We're going to use this application to demonstrate DevOps concepts:
  - Build
  - Test
  - Package
  - Deploy
  - Run
  - Monitor

#### When do you use it?

If you're not familiar with Python+Django, perfect!  DevOps engineers don't have to understand everything about everything.  Well-understood patterns for good practices can be applied to almost any code/build process.

---

Use your repo setup script to create a new repo.  Call it whatever you want.

```
REPO_NAME=mywebapp
repo_setup.sh ${REPO_NAME}
```

Take note of the `ssh_url_to_repo` value.

`REPO_URL=<<<ssh_url_to_repo>>>`

Clone the demo webapp into your repo, then change the remote git repository:

```
git clone git@gitlab.com:sofreeus/devopscamp-webapp.git ~/git/${REPO_NAME}
cd ~/git/${REPO_NAME}
git remote rm origin
git remote add origin ${REPO_URL}
```

Verify that your local copy of the repository is pointed at your freshly-minted GitLab repository:

`git remote -v`

Follow the instructions in the README.md to get the webapp running locally.
