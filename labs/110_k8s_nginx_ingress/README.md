# Kubernetes Nginx Ingress

#### What are we going to do in this lab?

Leverage the Ingress Controller and Ingress features of Kubernetes to route traffic to our service.

#### Why do we need to do this?

Vendor-provided load balancing options can be both expensive and challenging to manage.  Using a Kubernetes ingress controller can be more cost effective and gives us more control over things like rewrite rules, throttling, and SSL certificates.

#### When do you use it?

Use an ingress when you need to expose a service to consumers outside your K8s cluster.

---

### Make a git commit

Commit early.  Commit often.

This is a good time to take another snapshot of your webapp repository and back it up to the server.

- `git add -A`
- `git commit -m 'describe your changes'`
- `git push`

---

### Switch Service to NodePort

- GKE/GCP doesn't automatically delete the load balancer!
- Delete the load balancer manually
- What's the difference between ClusterIP and NodePort?

- Start with `webapp-service-loadbalancer.yaml`
- Change the service type to `NodePort`
- Add a `name: webport` for your service's port
- Name the file `webapp-service.yaml`
- Get rid of any other `webapp-service*` files

---

### Apply the ingress config

- Copy the `webapp-ingress.yaml` in this lab to your repo
  - `cp ~/git/DevOps-Camp/labs/110_k8s_nginx_ingress/webapp-ingress.yaml ~/git/${REPO_NAME}/k8s`
- Update the `.spec.rules.host` to your URL
  - `atom ~/git/${REPO_NAME}/k8s/webapp-ingress.yaml`
  - Host should be
    - `${REPO_NAME}.dev.sfsdevops.camp`
    - or `webapp.dev.${MACGUFFIN}.sfsdevops.camp`
  - Change the `ServicePort` from `8000` to `webport`
- Apply it to your cluster
  - `kubectl apply -f webapp-ingress.yaml`
- Check it out
  - `kubectl get ing`

`xdg-open ${REPO_NAME}.dev.sfsdevops.camp/admin`
