# Prometheus

Prometheus is a tool for monitoring and alerting.  It can be complicated to setup, but we just installed Helm and Tiller to facilitate just this kind of thing.

---

#### Pipeline for OTSS (Off the Shelf Software)

It might be tempting to `helm install --name prometheus stable/prometheus` on your cluster and call it good.

- How does the next guy know what you did?
- How do you upgrade it?
- Is there any non-manual way to make a configuration change?
- How do you ensure that your dev/qa/uat/integration/staging/production environments are similar?

We're going to use Helm + Gitlab CI to deploy Prometheus via CI pipeline.

Note that in this repository we only have (basically) two files:
1. A file storing the values for our Prometheus deploy
1. A Gitlab CI file

We're not downloading, storing, packaging, containerizing, or othering the Prometheus software.


- Create a new repository
  - `~/git/management_scripts/repo_setup.sh prometheus`
- Clone the `devopscamp-prometheus` repository to your machine
  - `git clone git@gitlab.com:sofreeus/devopscamp-prometheus.git ~/git/prometheus`
- Go there
  - `cd ~/git/prometheus`
- Remove the origin
  - `git remote remove origin`
- Point it at your new repo
  - `git remote add origin git@gitlab.com:${MACGUFFIN}/prometheus.git`
- Check out the readme
  - `xdg-open https://gitlab.com/${MACGUFFIN}/prometheus`
- Per the Happy Path instructions
  - Add the `DEV_PROMETHEUS_ADMIN_PASSWORD` and `PROD_PROMETHEUS_ADMIN_PASSWORD` variables to your repo's settings (settings --> CI/CD --> variables)
- Go to the CI/CD Pipelines page
  - Click the `Run Pipeline` button
  - Click `Create Pipeline`
- Watch it go!
- See if you can get to Prometheus
  - `xdg-open https://prometheus.dev.${MACGUFFIN}.sfsdevops.camp`
  - Log in as `admin` using the password you set in your variables
