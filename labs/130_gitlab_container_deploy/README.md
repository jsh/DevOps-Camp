# Deploy from CI

#### What are we going to do in this lab?

Why stop at generating an image?  Use GitLab CI to deploy a running app to Kubernetes.

#### Why do we need to do this?

We want to automate the full pipeline, and this is part of the pipeline.  Automating the deploy opens the door to conitnuous delivery - nay!  continuous deployment!

#### When do you use it?

Use this as a start toward developing your own standardized deployment process from your CI system.  Ideally, all applications at your organization will follow a similar - if not the same - deployment process.

---

### Prep - Reorganize

- Create a new repository for your Postgres service
  - `~/git/management_scripts/setup.sh ${REPO_NAME}-postgres`
    - Find the value of ssh_url_to_repo
- Clone this repo locally (you could add that to the script!)
  - `cd ~/git && git clone value_of_ssh_url_to_repo`
- Move your Postgres bits into that repo
  - `mkdir ~/git/${REPO_NAME}-postgres/k8s`
  - `mv ~/git/${REPO_NAME}/k8s/postgres* ~/git/${REPO_NAME}-postgres/k8s`
  - Commit and push your postgres repo

- Make sure the `k8s` directory of your webapp looks like this  
  ```
  k8s
  ├── webapp-deploy.yaml
  ├── webapp-ingress.yaml
  ├── webapp-init-job.yaml
  └── webapp-service.yaml
  ```

- Modify the Kubernetes manifests for your webapp to replace the following hard-coded values:
  - image --> `DOCKER_IMAGE`
  - `webapp.macguffin.sfsdevops.camp` --> `webapp.DOMAIN`

---

Note: Remember you and your partner have DEV and PROD versions of both GCP projects and GKE clusters.  Each student will generate one service account (either DEV or PROD), but will add two Gitlab variables.

### Create a service account for Gitlab

This enables Gitlab to perform deploys on your cluster.

1. cloud.google.com --> console --> IAM --> Service Accounts
1. Create Service Account --> `GitLabCI`
1. Role = `Kubernetes Engine Developer`
1. Check the `Furnish a new private key` checkbox; select JSON
1. Save

---

### Create a deploy token for Gitlab

This enables Kubernetes to read from your registry.

1. Gitlab --> your webapp repository --> Settings --> Repository --> Deploy Tokens
  1. Name it `${MACGUFFIN}-cluster` to match your cluster name
  1. Grant it `read_registry` scope
  1. Click `Create deploy token`
  1. Store the resulting credentials in Keypass
1. Create a secret on your cluster
  ```
  kubectl create secret docker-registry ${REPO_NAME}-registry \
    --docker-server=registry.gitlab.com \
    --docker-username="USERNAME_GOES_HERE" \
    --docker-password="PASSWORD_GOES_HERE" \
    --docker-email="does.not@matter.com"
  ```
1. Add this secret to the `webapp-deploy.yaml`
  1. Add this block to the `.spec.template.spec`
     ```
     imagePullSecrets:
     - name: ${REPO_NAME}-registry
     ```

---

### Configure GitLab Environment Variables

- Go to GitLab --> your GROUP (not repo) --> Settings --> CI/CD --> Variables
- Add authentication variables
  - `DEV_GCP_AUTH_KEYFILE`
  - `PROD_GCP_AUTH_KEYFILE`
  - Paste the entire contents of the corresponding JSON file into the value field.
- Add DOMAIN variables
  - `DEV_DOMAIN` = `dev.${MACGUFFIN}.sfsdevops.camp`
  - `PROD_DOMAIN` = `${MACGUFFIN}.sfsdevops.camp`
- While you're here, add the following variables as well...
  - Set each of these to the corresponding values for your cluster and your partner's cluster
  - `DEV_CLUSTER`
  - `DEV_PROJECT`
  - `DEV_REGION`
  - `PROD_CLUSTER`
  - `PROD_PROJECT`
  - `PROD_REGION`
- Save variables

---

### Make it work!

- Copy the `.gitlab-ci.yml` (name is an exact requirement) to the root of your repo
  - `cp ~/git/DevOps-Camp/labs/130_gitlab_container_deploy/.gitlab-ci.yml ~/git/${REPO_NAME}/`
- Examine the file
  - `atom ~/git/${REPO_NAME}/.gitlab-ci.yml`
- Gitlab will identify and process this file on every commit
  - Commit!
  - Push!
