#!/bin/bash -eu
# https://docs.gitlab.com/ce/api/projects.html

# This script requires that `curl` and `jq` are installed

GROUP_NAME="macguffin"

# Parse the command line argument to get the new project name
PROJECT_NAME=$1

# URL for the GitLab API
URL="https://gitlab.com/api/v4"

# Construct the header from the OS environment variables
AUTH_HEADER="Private-Token: ${GITLAB_API_KEY}"

# Namespace for your Macguffin group in GitLab
NAMESPACE_ID=$(curl --silent -H "${AUTH_HEADER}" ${URL}/namespaces/${GROUP_NAME} | jq '.id')

# Construct our arguments for the GitLab projects API
PAYLOAD="name=${PROJECT_NAME}&namespace_id=${NAMESPACE_ID}"

# Attempt the API call
RESPONSE=$(curl -X POST --silent -H "${AUTH_HEADER}" "${URL}/projects?${PAYLOAD}")

# Pretty print our response
echo ${RESPONSE} | jq

# Look for the project id in the response
# Use jq to return a 0 if there is no project ID
PROJECT_ID=$(echo ${RESPONSE} | jq '.id // 0')

# If PROJECT_ID is 0, we know the project did not create successfully
if [ "${PROJECT_ID}" = "0" ]
then
  echo "There was a problem creating ${PROJECT_NAME}"
  exit 1
else
  echo "${PROJECT_NAME} created successfully."
fi


# Share our new projects with our subgroups:

# Get group ID's
DEVELOPER=$(curl --silent -H "${AUTH_HEADER}" ${URL}/groups/${NAMESPACE_ID}/subgroups?search=Developers | jq '.[].id')
PRODUCT=$(curl --silent -H "${AUTH_HEADER}" ${URL}/groups/${NAMESPACE_ID}/subgroups?search=Product | jq '.[].id')

echo "Granting access to Developers..."
PAYLOAD="id=${PROJECT_ID}&group_id=${DEVELOPER}&group_access=30"
REQ_URL="${URL}/projects/${PROJECT_ID}/share"
RESPONSE=$(curl -X POST --silent -H "${AUTH_HEADER}" "${REQ_URL}?${PAYLOAD}")

# Pretty print our response
echo ${RESPONSE} | jq

# Grant access to Product team
echo "Granting access to Product..."
PAYLOAD="id=${PROJECT_ID}&group_id=${PRODUCT}&group_access=20"
REQ_URL="${URL}/projects/${PROJECT_ID}/share"
RESPONSE=$(curl -X POST --silent -H "${AUTH_HEADER}" "${REQ_URL}?${PAYLOAD}")

# Pretty print our response
echo ${RESPONSE} | jq
