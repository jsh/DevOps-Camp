# Git Server Management

#### What are we going to do in this lab?

We're going to use a combination of manual setup and API scripting to manage GitLab repositories.

#### Why do we need to do this?

- Automation will make the process repeatable, fast, and consistent
- Role-based access control (RBAC) is a very good practice for security
- API scripting is a critical tool in a DevOps tool belt

#### When do you use it?

- Startups: Speed
- Enterprise: Security
- GitLab, GitHub, BitBucket
- Documentation


---

## Managing Repository Access

1. Create an account on the GitLab Server
1. [Create a group](https://gitlab.com/groups/new) for your Macguffin
1. Create a new project, `example-project`, under that group
1. How do we give role-based access to the code?
1. Under your main group, create two subgroups: Developers and Product
    - Browse to https://gitlab.com/${MACGUFFIN}
    - Click the triangle to the right of "New project"
    - Choose "New subgroup" menu-item
    - Click the "New subgroup" button
1. Share `example-project`
  - Developers should have Developer access
  - Product should have Reporter access


## Let's automate it!
- [GitLab API Reference](https://docs.gitlab.com/ce/api/)

1. Generate an api-scope [personal access token](https://gitlab.com/profile/personal_access_tokens) (SAVE IT!!!)
1. Configure your personal access token in your shell environment:
  - `export GITLAB_API_KEY=<your-access-token>`
1. Jump in to the repo_setup.sh script...
  - `atom ~/git/DevOps-Camp` to open the entire repo in Atom.
  - Navigate to `DevOps-Camp/labs/010_git_repo_setup/repo_setup.sh`
  - You may want to go to Atom's preferences --> Packages --> autocomplete-plus --> settings and un-check `Show suggestions on keystroke`
1. Edit the file to add your group name
1. Run the script:
  - `cd ~/git/DevOps-Camp/labs/010_git_repo_setup/`
  - `./repo_setup.sh my-automated-repo`
1. Oh, no!  Using Atom, find the `get-reqd.yml` file in the 000_setup lab
  - Add package entries for `jq` and `curl`
  - `ansible-playbook ~/git/DevOps-Camp/labs/000_setup/get-reqd.yml`
1. `./repo_setup.sh my-automated-repo`

1. Create a new repository called `management_scripts`
  - Clone it
  - Add `repo_setup.sh`
  - Commit
  - Push
