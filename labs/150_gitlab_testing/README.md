# Coverage Test Report

#### What are we going to do in this lab?

Add a test coverage report to our CI run.

#### Why do we need to do this?

Adding a coverage test helps shows the status of overall test automation at your organization.  But in this case, it helps us think about what's in the image, and how and when we run tests against our code.

#### When do you use it?

Ideally, you want to test _exactly_ the Thing that's going to be running in production.  If that's a Docker container, you want to test the container.  But if this means you have to add a test framework into your container, you might invent a compromise.

---

Add this block as a new job in your `.gitlab-ci.yaml`:

```
coverage_test:
  image: python:3.6.5
  services:
    - name: postgres:10.4
      alias: db
  script:
    - pip install -r requirements.txt
    - pip install coverage
    - cd mysite
    - python manage.py migrate
    - coverage run --source=. manage.py test polls
    - coverage report
```

This does a number of things differently from our first CI job:

- It runs in the `python:3.6.5` image instead of the `docker:stable` image
- It doesn't pull, build, or push any docker images
- It includes a `service` for the database instead of running it as part of the job
- It installs and runs the coverage test

Doing it this way means that any modules associated with the `pip install coverage` command won't end up in our final artifact.  We're still performing automated functional testing against our final artifact in the other job, so this is probably an acceptable compromise.
