# Build an EKS cluster

#### What are we going to do in this lab?

Build a Kubernetes cluster in Amazon, using Amazon Elastic Container Service for Kubernetes (EKS).

#### Why do we need to do this?

Kubernetes is _hard_.  But oh so good.  We've built containers locally.  We've run them with `docker run ...` and `docker-compose`, but how would we run a container on a server?  Docker Swarm, Mesosphere Marathon, and Kubernetes are all answers to this question, but Kubernetes is unquestionably the leader in this space.

#### When do you use it?

If you have it, always.  Kubernetes can run single-instance services.  It can run scaling services.  It can run one-off batch jobs.  It can provide self-healing and rolling updates.

---

## Make a K8s!

- Deploy our basic VPC stack in Oregon (if it's missing)
  - EKS is only available in N.VA and OR.  OR is cheaper.


###### In case we need it, here's the [EKS Getting Started](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html) guide.

(They swapped out the Heptio authenticator for an aws-iam-authenticator while this lab was being developed.)

Create an EKS service role

- Open the IAM console at https://console.aws.amazon.com/iam/.
- Choose Roles, then Create role.
- Choose EKS from the list of services, then Allows Amazon EKS to manage your clusters on your behalf for your use case, then Next: Permissions.
- Choose Next: Review.
- For Role name, enter `your_macguffin-eksServiceRole`, then choose Create role.



```
export MACGUFFIN=ark
export CLUSTER_NAME=${MACGUFFIN}
```

Get some required values from our CloudFormation stacks:

```
export SUBNETS=$(aws --region us-west-2 cloudformation list-exports | jq -r '.Exports[].Value' | grep ${MACGUFFIN} | grep subnet | xargs | tr ' ' ',')
export SG_ADMIN=$(aws --region us-west-2 cloudformation list-exports | jq -r '.Exports[] | select(.Name=="${MACGUFFIN}-vpc-AdminSshSecurityGroup").Value')
export SG_DEV=$(aws --region us-west-2 cloudformation list-exports | jq -r '.Exports[] | select(.Name=="${MACGUFFIN}-vpc-DevSecurityGroup").Value')
export SG_PROD=$(aws --region us-west-2 cloudformation list-exports | jq -r '.Exports[] | select(.Name=="${MACGUFFIN}-vpc-PRODSecurityGroup").Value')
```

Prepare our VPC configuration and grab the ARN of our macguffin's EKS service role:

```
export VPC_CONFIG="subnetIds=${SUBNETS}securityGroupIds=${SG_DEV},${SG_ADMIN}"

export ROLE_ARN=$(aws iam list-roles | jq -r '.Roles[] | select(.RoleName=="eksServiceRole").Arn' | grep ${MACGUFFIN})
```

Build our EKS cluster!

```
aws --region us-west-2 eks create-cluster --name ${CLUSTER_NAME} --role-arn ${ROLE_ARN} --resources-vpc-config ${VPC_CONFIG}

# Wait for cluster creation...
watch -n 15 aws eks describe-cluster --name ${CLUSTER_NAME} --query cluster.status
```

---

### Configure your client environment

###### Install kubectl

1. Install `kubectl`
  - Ubuntu: `snap install kubectl --classic`
  - MacOS: `brew install kubernetes-cli`
1. Verify `kubectl`
  - `kubectl version --short --client`

###### Install the aws-iam-authenticator

- Download the binary
  - `curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/linux/amd64/aws-iam-authenticator`
- Make it executable
  - `chmod +x ./aws-iam-authenticator`
- Put it in your path
  - `mkdir -p $HOME/bin && cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$HOME/bin:$PATH`
- Make sure to set your path every login
  - `echo 'export PATH=$HOME/bin:$PATH' >> ~/.bashrc`
- Verify that it installed correctly
  - `aws-iam-authenticator help`

###### Add the kubectl config for your cluster

- Copy the `config-template` file in this lab to `~/.kube/config-${MACGUFFIN}` on your machine.
- Edit `~/.kube/config-${MACGUFFIN}` to replace `<endpoint-url>`, `<base64-encoded-ca-cert>`, and `<cluster-name>`.
- You know the cluster name, but here's how to get the other two:
  ```
  aws eks describe-cluster --name ${CLUSTER_NAME} --query cluster.endpoint
  aws eks describe-cluster --name ${CLUSTER_NAME} --query cluster.certificateAuthority.data
  ```
- Now we have to tell `kubectl` to use this config:
  ```
  export KUBECONFIG=$KUBECONFIG:~/.kube/config-${MACGUFFIN}
  echo 'export KUBECONFIG=$KUBECONFIG:~/.kube/config-${MACGUFFIN}' >> ~/.bashrc
  ```

At this point, we've only created the control plane for our cluster.  There are no actual worker nodes in our cluster.  We have to add those:

---

#### Add workers to cluster

Amazon provides a CloudFormation template for this.  Instead of running it from the console, we're going to run it from the command line.  We're using some of the variables from the previous section.  Make sure that `${MACGUFFIN}` and `${SG_DEV}` are still set.

- Set a few new variables for this stack
  ```
  export VPC=$(aws --region us-west-2 cloudformation list-exports | jq -r '.Exports[] | select(.Name=="basic-vpc-VpcId").Value')
  export PRIVATE_SUBNETS=$(aws --region us-west-2 cloudformation list-exports | jq -r '.Exports[] | select(.Name | contains("PrivSub")).Value' | xargs | tr ' ' ',')
  ```
- This is the template from Amazon.  We're just storing it to make our CLI command a bit more sane later.
  ```
  export TEMPLATE="https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/amazon-eks-nodegroup.yaml"
  ```
- These are the parameters for the stack
  ```
  export PARAMETERS="ParameterKey=KeyName,ParameterValue=devopscampdemo
    ParameterKey=NodeImageId,ParameterValue=ami-73a6e20b
    ParameterKey=NodeInstanceType,ParameterValue=t2.small
    ParameterKey=NodeAutoScalingGroupMinSize,ParameterValue=2
    ParameterKey=NodeAutoScalingGroupMaxSize,ParameterValue=2
    ParameterKey=ClusterName,ParameterValue=${CLUSTER_NAME}
    ParameterKey=NodeGroupName,ParameterValue=eks-worker
    ParameterKey=ClusterControlPlaneSecurityGroup,ParameterValue=${SG_DEV}
    ParameterKey=VpcId,ParameterValue=${VPC}
    ParameterKey=Subnets,ParameterValue=\\\\\"${PRIVATE_SUBNETS}\\\\\""
  ```
- Now we put it all together and create the stack
  ```
  aws cloudformation create-stack \
      --stack-name ${MACGUFFIN}-k8s-workers \
      --template-url ${TEMPLATE} \
      --parameters $(echo ${PARAMETERS} | xargs) \
      --capabilities CAPABILITY_IAM
  ```

###### We have a Kubernetes cluster!

---

#### Here's one more bit to integrate Amazon authentication with our cluster

Get the IAM Role for the Node Instances

- `aws cloudformation describe-stacks --stack-name ${MACGUFFIN}-k8s-workers | jq -r '.Stacks[].Outputs[] | select(.OutputKey=="NodeInstanceRole").OutputValue'`

Download and edit the AWS authenticator configmap:

- `curl -O https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-06-05/aws-auth-cm.yaml`

---

### Add an Nginx ingress controller to the cluster
- Scaffolding for the ingress
  - `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml`
- ingress controller service
  - `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/cloud-generic.yaml`
