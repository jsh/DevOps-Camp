# Service-Oriented Architecture

A.K.A. Microservices*

#### What are we going to do in this lab?

The Django app is self-contained right now.  We're going to create a PostgreSQL database to store our application's data.

#### Why do we need to do this?

- Simpler distributed coding
- Allows us to manage stateful data as a service
- We can manage bottlenecks independently
- Isolated impacts

#### When do you use it?

Many applications start life as a monolith.  At some point, there might be a particular function within an app that could be used elsewhere.

There are some basic principles - like separating the database from the application - that are much easier to follow up-front.

---

PROBLEM: Our app uses a SQLite DB to store stateful data.  We need to move it.

Let's use PostgreSQL!

1. Generate a "random" password for Postgres:
  - `export POSTGRES_PASSWORD=$(pwgen -y 16 1)`
1. Create a Docker volume that will hold persistent data:
  - `docker volume create pgsql_data`
1. Run a Postgres server
  - `docker run -d --rm --name db -v pgsql_data:/var/lib/postgresql/data -e POSTGRES_PASSWORD:$POSTGRES_PASSWORD -p 5432:5432 postgres:10.4`
  - Same-same: `docker run -d --rm --name db -v pgsql_data:/var/lib/postgresql/data -e POSTGRES_PASSWORD -p 5432:5432 postgres:10.4`

Change the Django settings

1. Edit the `mysite/mysite/settings.py` file
1. Comment out the sqlite block
1. Uncomment the Postgres block

Add the appropriate environment variables for the webapp

```
export DJANGO_DB_USER=postgres
export DJANGO_DB_PASS=$POSTGRES_PASSWORD
export DJANGO_DB_HOST=localhost
```

Try to run the app:

1. `python3 manage.py runserver`
1. [Check it out](http://localhost:8000/polls)
1. Uh-oh!  There's no data!
1. `python3 manage.py migrate`

Set up our stuff (again. ugh.)
1. `python3 manage.py createsuperuser`
