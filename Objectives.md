- Stitch together disparate applications with API scripting
- Do more with less; manage your infrastructure as code
- Use containers for developer freedom and operational consistency
- Scale out simply with orchestration tools that can eliminate maintenance windows
- Build a decentralized, self-service monitoring infrastructure
- Leverage your DevOps skill set to build a continuous integration (CI/CD) pipeline


- API Scripting
    - bash
    - python w/ boto3+requests (so much faster than bash)

- Version Control >>> Assuming existing knowledge
    - git
    - GitLab

- Infrastructure as Code
    - HEAT templates?  (I don't know this; would have to learn; would require our own infra)
        - VPC
        - Subnets
        - Routes
        - Gateways
        - Security groups
        - Instances
        - Scaling groups
        - DNS entries

- Configuration Management
    - Ansible

- Architecting for Scale
    - Immutable infrastructure
    - Load balancing

- Containers
    - Docker

- Continuous Integration
    - GitLab CI

---

Day 1
- Set up git repo
- Infrastructure as code
    - VPC
    - Subnets
    - Routes
    - Gateways
    - Security groups
    - Instances
    - Scaling groups
    - DNS entries

Day 2
- Sensu in Docker?
    - Monitoring
    - API scripting
    - Docker
- Configuration Management?

Day 3
- CI/CD Pipelines
    - Build
    - Test
    - Deploy

Day 4
- Docker containers in the cloud...
    - Kubernetes?  Minikube?  Murano?
    - Swarm?
    - Mesos + Marathon?

---

- Start with students running Git from a fileserver.
- Silvia teaches HEAT templates to provision infrastructure for K8s.
- Set up K8s on OpenStack.
- Deploy GitLab as a K8s app, then migrate repos.
- Implement a demo app in GitLab+K8s, via GitLab CI
- Add a monitoring solution (Sensu if not Prometheus)

---

## Out of scope

- ELK Stack
- AWS/GCE/DO proprietary extensions

