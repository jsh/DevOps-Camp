To enable students to play with LetsEncrypt...

1. Every student creates a Macguffin
1. Every pair of students creates a DEV and PROD k8s cluster (eventually)
1. Add a DNS A record to sfsdevops.camp for every Macguffin:
  - EggplantDEV ingress  is 10.7.8.9
  - EggplantPROD ingress is 10.13.14.15
  - Create DEV records for macguffins at Gandi:
    - *.dev.chalice.sfsdevops.camp --> 10.7.8.9
    - *.dev.miners.sfsdevops.camp --> 10.7.8.9
  - Create PROD records for macguffins at Gandi:
    - *.chalice.sfsdevops.camp --> 10.13.14.15
    - *.miners.sfsdevops.camp --> 10.13.14.15
